import stockgame.ShareDepositAccount;
import stockgame.ShareItem;
import stockgame.Share;
import utils.LinkedList;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Test;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * Test-File for ShareDespoitAccount
 *
 */

public class ShareDepositAccountTest {

	Share share = new Share("BMW", 5);
	ShareItem shareItem = new ShareItem(share, 10);
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}
	
	@BeforeClass 
	public static void setUpBeforeClass() {
	}
	
	@Test
	public void testAddShareItem() {
		ShareDepositAccount shareDepositAccount = new ShareDepositAccount("Nummer1");
		shareDepositAccount.addShareItem(shareItem);
		assertEquals("Wert = 50", shareDepositAccount.getValue(), 50);
	}
	
	@Test
	public void testRemoveShareItem() {
		ShareDepositAccount shareDepositAccount = new ShareDepositAccount("Nummer1");
		shareDepositAccount.addShareItem(shareItem);
		assertEquals("Wert = 50", shareDepositAccount.getValue(), 50);
		shareDepositAccount.removeShareItem(shareItem);
		assertEquals("Wert = 0", shareDepositAccount.getValue(), 0);
	}
	
	@Test
	public void testCalcWinLossDeltaForShare() {
		assertEquals("Wert = 0", shareItem.calcWinLossDelta(), 0);
	}
}
