import stockgame.CashAccount;
import stockgame.exceptions.NotEnoughMoneyException;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Test;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * Test-File for CashAccount
 *
 */

public class CashAccountTest {
	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
		
	}

	@BeforeClass
	public static void setUpBeforeClass() {
		
	}

	@AfterClass
	public static void tearDownAfterClass() {
		
	}

	@Test
	public void testConstructor() {
		CashAccount cashAccount = new CashAccount("test", 20);
		assertEquals("Wert = 20", cashAccount.getValue(), 20);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testAddMoney() {
		CashAccount cashAccount = new CashAccount("test", 20);
		cashAccount.addMoney(30);
		assertEquals("20 + 30 = 50", cashAccount.getValue(), 50);
		cashAccount.addMoney(-10);
	}

	@Test(expected=NotEnoughMoneyException.class)
	public void testRemoveMoney1() throws Throwable {
		CashAccount cashAccount = new CashAccount("test", 20);
		cashAccount.removeMoney(10);
		assertEquals("20 - 10 = 10", cashAccount.getValue(), 10);
		cashAccount.removeMoney(20);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testRemoveMoney2() throws Throwable {
		CashAccount cashAccount = new CashAccount("test", 20);
		cashAccount.removeMoney(-10);
	}
}
