import stockgame.Share;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Test;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * Test-File for Share
 *
 */

public class ShareTest {
	
	Share share = new Share("GOOGLE", 10);
	Share share2 = new Share("BMW", 10);
	Share share3 = new Share("BMW", 10);

	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}
	
	@BeforeClass 
	public static void setUpBeforeClass() {
	}
	
	@Test
	public void testSetCourse() {
		share.setCourse(100);
		share2.setCourse(50);
		assertEquals("Kurs = 100", share.getCourse(), 100);
		assertEquals("Kurs = 50", share2.getCourse(), 50);
	}
	
	@Test
	public void testEquals() {
		assertTrue("share != share2", share.getName().equals(share2.getName()) == false);
		assertTrue("share2 == share3", share2.getName().equals(share3.getName()) == true);
	}
	
}
