import stockgame.Player;
import stockgame.exceptions.NotEnoughMoneyException;
import stockgame.exceptions.NotEnoughSharesException;
import stockgame.Share;
import stockgame.ShareItem;
import stockgame.CashAccount;
import stockgame.ShareDepositAccount;
import utils.LinkedList;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Test;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * Test-File for Player
 *
 */

public class PlayerTest {
	
	Player andreas = new Player("Andreas", 5000);
	Player michael = new Player("Michael", 5000);
	
	Share share = new Share("BMW", 10);
	ShareItem shareItem = new ShareItem(share, 5);
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}
	
	@BeforeClass 
	public static void setUpBeforeClass() {
	}
	
	@Test
	public void testCalcWinLossDelta() throws Throwable {
		Player michael = new Player("Michael", 100000);
		michael.buyShares(share, 10);
		share.setCourse(100);
		assertEquals("Gewinn = ", michael.calcWinLossDeltaForShare(share), 900);
	}
	
	@Test(expected=NotEnoughMoneyException.class)
	public void testBuyShares() throws Throwable{
		Share share = new Share("GOOGLE", 10);
		Share share2 = new Share("BMW", 10);
		andreas.buyShares(share, 3);
		assertEquals("Vermögen = 4970", andreas.getMoney(), 4970);
		michael.buyShares(share2, 8);
		assertEquals("Vermögen = 4920", michael.getMoney(), 4920);
		andreas.buyShares(share, 80000);
	}
	
	@Test(expected=NotEnoughSharesException.class)
	public void testSellShares() throws Throwable{
		Share share = new Share("GOOGLE", 10);
		Share share2 = new Share("BMW", 10);
		andreas.sellShares(share, 3);
		assertEquals("Vermögen = 4970", andreas.getMoney(), 4970);
		michael.sellShares(share2, 1);
		assertEquals("Vermögen = 4920", michael.getMoney(), 4920);
		andreas.sellShares(share, 12000000);
	}
	
}
