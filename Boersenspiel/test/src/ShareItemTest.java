import stockgame.ShareItem;
import stockgame.Share;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Test;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * Test-File for ShareItem
 *
 */

public class ShareItemTest {

	Share share = new Share("BMW", 10);
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}
	
	@BeforeClass 
	public static void setUpBeforeClass() {
	}
	
	@Test
	public void testCalcWinLossDelta() {
		ShareItem shareItem = new ShareItem(share, 5);
		assertEquals("Unterschied = 0", shareItem.getValue() - shareItem.getBuyValue(), 0);
	}
	
	@Test
	public void testSetCount() {
		ShareItem shareItem = new ShareItem(share, 5);
		assertEquals("5*10=50", shareItem.getValue(), 50);
		shareItem.setCount(10);
		assertEquals("10*10=100", shareItem.getValue(), 100);
	}
	
}
