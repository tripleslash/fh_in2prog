package utils;
import java.util.Timer;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * GlobalTimer
 *
 */

public class GlobalTimer {
	private static Timer s_timerInstance = null;

	public static Timer getInstance() {
		if (s_timerInstance == null)
			s_timerInstance = new Timer();
		return s_timerInstance;
	}
}
