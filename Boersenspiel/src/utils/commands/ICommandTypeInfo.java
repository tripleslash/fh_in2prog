package utils.commands;

import java.lang.reflect.Method;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * Interface ICommandTypeInfo
 *
 */

public interface ICommandTypeInfo {

	String getName();
	void setName(String name);

	String getHelpText();
	void setHelpText (String helpText);

	Method getMethod();
	void setMethod(Method method);

	Object getTarget();
	void setTarget(Object target);
}
