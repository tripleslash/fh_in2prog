package utils.commands;

import java.lang.reflect.Method;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * CommandDescriptor, implements ICommandTypeInfo
 *
 */

public class CommandDescriptor implements ICommandTypeInfo {
	private String m_commandName = null;
	private String m_helpText = null;
	private Method m_method = null;
	private Object m_targetClassObject = null;

	public CommandDescriptor(String name, String helpText, Method method, Object target) {
		m_commandName = name;
		m_helpText = helpText;
		m_method = method;
		m_targetClassObject = target;
	}

	@Override
	public String getName() {
		return m_commandName;
	}

	@Override
	public String getHelpText() {
		return m_helpText;
	}

	@Override
	public Method getMethod() {
		return m_method;
	}

	@Override
	public Object getTarget() {
		return m_targetClassObject;
	}

	@Override
	public void setName(String name) {
		m_commandName = name;
	}

	@Override
	public void setHelpText(String helpText) {
		m_helpText = helpText;
	}

	@Override
	public void setMethod(Method method) {
		m_method = method;
	}

	@Override
	public void setTarget(Object target) {
		m_targetClassObject = target;
	}
}
