package utils.commands;

import utils.IExecutable;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * CommandDescriptor, InvocationHandler
 *
 */

public class Command extends CommandDescriptor implements IExecutable {
	private InvocationHandler m_invocationHandler = null;

	public Command(String name, String helpText, Method method,
			Object target, InvocationHandler handler)
	{
		super(name, helpText, method, target);
		m_invocationHandler = handler;
	}

	@Override
	public Object execute(Object[] params) throws InvokeFailedException {
		try {
			// Falls ein InvocationHandler an das Kommando gebunden ist, benutze diesen f�r den Methodenaufruf
			if (m_invocationHandler != null)
				return m_invocationHandler.invoke(this, getMethod(), params);

			// Sonst: Fall-back zum Standard Reflection Aufruf
			return getMethod().invoke(getTarget(), params);
		}
		catch (Throwable e) {
			throw new InvokeFailedException(e);
		}
	}
}
