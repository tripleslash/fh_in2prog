package utils.commands;

import java.lang.annotation.*;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * AsCommand
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AsCommand {
	String name();
	String description() default "Keine Beschreibung vorhanden!";
}
