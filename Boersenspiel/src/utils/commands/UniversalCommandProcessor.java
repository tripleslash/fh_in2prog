package utils.commands;

import utils.LinkedList;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationHandler;
import java.util.logging.Logger;
import java.util.logging.Level;
import utils.MimeType;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * Universal CommandProcessor to use some commands with the program
 *
 */

public class UniversalCommandProcessor {
	public static class NoTargetException extends RuntimeException {
		public NoTargetException() {
			super("Kommandoprozessor hat keine Zielklasse! Erst attachToTarget benutzen.");
		}
	}

	public static class CommandNotFoundException extends Exception {
		public CommandNotFoundException(String command) {
			super("Kommando \"" + command + "\" nicht gefunden! Versuchs mit \"help\".");
		}
	}

	public static class ArgumentCountMismatchException extends Exception {
		public ArgumentCountMismatchException(String command) {
			super("Argumentanzahl f�r \"" + command + "\" passt nicht!");
		}
	}

	private Object m_targetClassObject;
	private InvocationHandler m_invocationHandler;
	private Command[] m_registeredCommands;

	private BufferedReader m_shellReader;
	private PrintStream m_shellWriter;

	private static Logger s_logger = Logger.getLogger(UniversalCommandProcessor.class.getName());

	public UniversalCommandProcessor() {
		m_shellWriter = System.out;
		m_shellReader = new BufferedReader(new InputStreamReader(System.in));
	}

	public UniversalCommandProcessor(InputStream input, PrintStream output) {
		m_shellWriter = output;
		m_shellReader = new BufferedReader(new InputStreamReader(input));
	}

	public void attachToTarget(Object target, InvocationHandler handler) {
		m_targetClassObject = target;
		m_invocationHandler = handler;
		m_registeredCommands = collectCommands(target);
	}

	public Object getTarget() {
		return m_targetClassObject;
	}

	private Command[] collectCommands(Object target) {
		LinkedList<Command> commandTypes = new LinkedList<Command>();
		for (Method method : target.getClass().getMethods()) {
			AsCommand annotation = method.getAnnotation(AsCommand.class);
			if (annotation == null)
				continue;

			commandTypes.pushBack(new Command(annotation.name(),
				annotation.description(), method, target, m_invocationHandler));
		}

		return commandTypes.asArray(Command.class);
	}

	private Command findCommand(String commandName) {
		for (Command command : m_registeredCommands) {
			if (command.getName().equals(commandName))
				return command;
		}
		return null;
	}

	private String argumentTypesToString(Class<?>[] parameterTypes) {
		boolean firstParam = true;
		StringBuilder methodSignature = new StringBuilder();
		for (Class<?> paramType : parameterTypes) {
			if (!firstParam)
				methodSignature.append(", ");
			firstParam = false;
			methodSignature.append(paramType.toString());
		}
		return methodSignature.toString();
	}

	private String[] parseCommand(String line, char delim, boolean ignoreEmptyParts) {
		boolean isString = false; // "
		boolean isSingleString = false; // '
		boolean isEscaped = false;

		StringBuilder current = new StringBuilder();
		LinkedList<String> splitParts = new LinkedList<String>();

		for (char c : line.toCharArray()) {
			switch (c) {
			case '\\':
				if (isEscaped || (!isString && !isSingleString))
					current.append(c);
				isEscaped = !isEscaped;
				break;
				
			case '\"':
				if (isSingleString || isEscaped)
					current.append(c);
				else
					isString = !isString;
				break;
				
			case '\'':
				if (isString || isEscaped)
					current.append(c);
				else
					isSingleString = !isSingleString;
				break;
				
			default:
				if (c != delim)
					current.append(c);
				else {
					if (isEscaped || isString || isSingleString)
						current.append(c);
					
					else if (!ignoreEmptyParts || current.length() > 0) {
						splitParts.pushBack(current.toString());
						current.setLength(0);
					}
				}
			}

			if (c != '\\')
				isEscaped = false;
		}

		if (current.length() > 0)
			splitParts.pushBack(current.toString());
		return splitParts.asArray(String.class);
	}

	public String getHelpText() {
		StringBuilder helpText = new StringBuilder();
		helpText.append("Verf�gbare Kommandos:\n");
		for (Command command : m_registeredCommands)
			helpText.append(" - \"" + command.getName() + "\" (" + command.getHelpText() + ")\n");
		return helpText.toString();
	}

	public void pollNext() {
		if (m_targetClassObject == null)
			throw new NoTargetException();

		Class<?>[] paramTypes = null;

		try {
			String currentLine = m_shellReader.readLine();
			String[] splitParts = parseCommand(currentLine, ' ', true);
			if (splitParts.length < 1)
				return;

			// Hardcoded Hilfe
			String commandString = splitParts[0];
			if (commandString.equals("help")) {
				m_shellWriter.print(getHelpText());
				return;
			}

			Command command = findCommand(commandString);
			if (command == null)
				throw new CommandNotFoundException(commandString);

			paramTypes = command.getMethod().getParameterTypes();
			Object[] parsedParams = new Object[paramTypes.length];
			if (paramTypes.length + 1 != splitParts.length)
				throw new ArgumentCountMismatchException(commandString);

			int i = 0;
			for (Class<?> type : paramTypes) {
				String currentParam = splitParts[i + 1];
				if (type == byte.class || type == Byte.class)
					parsedParams[i++] = Byte.decode(currentParam);

				else if (type == short.class || type == Short.class)
					parsedParams[i++] = Short.decode(currentParam);

				else if (type == int.class || type == Integer.class)
					parsedParams[i++] = Integer.decode(currentParam);

				else if (type == long.class || type == Long.class)
					parsedParams[i++] = Long.decode(currentParam);

				else if (type == float.class || type == Float.class)
					parsedParams[i++] = Float.parseFloat(currentParam);

				else if (type == double.class || type == Double.class)
					parsedParams[i++] = Double.parseDouble(currentParam);

				else if (type == boolean.class || type == Boolean.class)
					parsedParams[i++] = Boolean.parseBoolean(currentParam);

				else if (type == MimeType.class)
					parsedParams[i++] = MimeType.parseMime(currentParam);

				else
					parsedParams[i++] = currentParam;
			}

			// Das Kommando ausf�hren. Ausgabe ist dem InvocationHandler �berlassen.
			command.execute(parsedParams);
		}
		catch (Throwable e) {
			if (paramTypes == null) {
				s_logger.log(Level.WARNING, "Ausnahme im Kommandoprozessor!", e);
				return;
			}

			String argumentTypes = argumentTypesToString(paramTypes);
			s_logger.log(Level.WARNING, "Ausnahme im Kommandoprozessor! Argumenttypen sind: " + argumentTypes, e);
		}
	}
}

