package utils;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * ItemNotFoundException
 *
 */

public class ItemNotFoundException extends RuntimeException {
	public ItemNotFoundException(String what) {
		super(what);
	}
}
