package utils.logging;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Logger;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * Logger for the Program
 *
 */

public class InvocationLogger implements InvocationHandler {
	private static Logger s_logger = Logger.getLogger(InvocationLogger.class.getName());
	private Object m_targetClassObject = null;

	public InvocationLogger(Object target) {
		m_targetClassObject = target;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		StringBuilder logMessage = new StringBuilder();
		logMessage.append("Methodenaufruf: " + method + "\n");

		if (args != null) {
			boolean firstArg = true;
			logMessage.append(" - Argumente: ");

			for (Object argument : args) {
				logMessage.append(firstArg ? argument : ", " + argument);
				firstArg = false;
			}
			logMessage.append("\n");
		}

		try {
			Object result = method.invoke(m_targetClassObject, args);
			if (result != null) {
				Class<?> returnType = result.getClass();
				if (returnType != void.class && returnType != Void.class)
					logMessage.append(" - R�ckgabewert: " + result.toString());
			}
			s_logger.info(logMessage.toString());
			return result;
		}
		catch (InvocationTargetException e) {
			logMessage.append(" - Ausnahme: " + e.getTargetException());
			s_logger.warning(logMessage.toString());
			throw e.getTargetException();
		}
	}
}

