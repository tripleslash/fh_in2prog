package utils;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * ItemDuplicateException
 *
 */

public class ItemDuplicateException extends RuntimeException {
	public ItemDuplicateException(String what) {
		super(what);
	}
}
