package utils;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * Interface for IExecutable
 *
 */

public interface IExecutable {
	public static class InvokeFailedException extends Exception {
		public InvokeFailedException(String what) {
			super(what);
		}

		public InvokeFailedException(Throwable what) {
			super(what);
		}
	}

	Object execute(Object[] params) throws InvokeFailedException;
}
