package utils;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * File for the Mime-Type, to filter some Mime-Types.
 *
 */

public enum MimeType {
	TextPlain("text/plain"),
	TextHtml("text/html");

	public final String contentType;

	private MimeType(final String type) {
		this.contentType = type;
	}

	public static class TypeNotSupportedException extends RuntimeException {
		public TypeNotSupportedException(String mimeType) {
			super("Mime-Type \"" + mimeType + "\" wird von der Anwendung nicht unterst�tzt!");
		}
	}

	public static MimeType parseMime(String contentType) {
		String lower = contentType.toLowerCase();
		for (MimeType mimeType : values()) {
			if (lower.equals(mimeType.contentType))
				return mimeType;
		}

		throw new TypeNotSupportedException(contentType);
	}
}
