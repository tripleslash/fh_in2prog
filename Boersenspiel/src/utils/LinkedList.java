package utils;

import java.lang.reflect.Array;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * LinkedList to hold Shares, Players. Ability to generate an Array.
 *
 */

public class LinkedList<T> {
	public class LinkNode {
		public T linkElement = null;
		public LinkNode prevLink = null;
		public LinkNode nextLink = null;
	}

	private LinkNode m_firstLink = null;
	private LinkNode m_lastLink = null;

	public LinkedList() {
		m_firstLink = new LinkNode();
		m_lastLink = new LinkNode();

		m_firstLink.nextLink = m_lastLink;
		m_lastLink.prevLink = m_firstLink;
	}

	public boolean isEmpty() {
		return m_firstLink.linkElement == null;
	}

	public T get(int idx) {
		int i = 0;
		for (LinkNode itr = m_firstLink;
				itr != m_lastLink; itr = itr.nextLink)
		{
			if (i++ == idx)
				return itr.linkElement;
		}

		return null;
	}

	public T front() {
		return m_firstLink.linkElement;
	}

	public T back() {
		return m_lastLink.prevLink.linkElement;
	}

	public LinkNode begin() {
		if (m_firstLink.linkElement != null)
			return m_firstLink;
		return m_lastLink;
	}

	public LinkNode end() {
		return m_lastLink;
	}

	public int getSize() {
		if (isEmpty())
			return 0;

		int i = 0;
		for (LinkNode itr = begin(); itr != end(); itr = itr.nextLink)
			++i;
		return i;
	}

	public void insert(int idx, T item) throws IllegalArgumentException {
		if (item == null)
			throw new IllegalArgumentException("Item kann nicht null sein!");

		// insertItem mit Index 0 ist das gleiche wie pushFront
		if (idx == 0) {
			pushFront(item);
			return;
		}

		int i = 1;
		for (LinkNode itr = m_firstLink.nextLink;
				itr != m_lastLink; itr = itr.nextLink)
		{
			if (i++ != idx)
				continue;

			// Neuen Link erzeugen
			LinkNode curLink = new LinkNode();
			curLink.linkElement = item;
			curLink.prevLink = itr.prevLink;
			curLink.prevLink.nextLink = curLink;
			curLink.nextLink = itr;
			itr.prevLink = curLink;
			return;
		}

		// Ungültiger Index -> pushBack
		pushBack(item);
	}
	
	public void pushFront(T item) throws IllegalArgumentException {
		if (item == null)
			throw new IllegalArgumentException("Item kann nicht null sein!");

		if (m_firstLink.linkElement == null) {
			m_firstLink.linkElement = item;
			return;
		}

		LinkNode curLink = new LinkNode();
		curLink.linkElement = item;
		curLink.nextLink = m_firstLink;
		m_firstLink.prevLink = curLink;
		m_firstLink = curLink;
	}

	public void pushBack(T item) throws IllegalArgumentException {
		if (item == null)
			throw new IllegalArgumentException("Item kann nicht null sein!");

		if (m_firstLink.linkElement == null) {
			m_firstLink.linkElement = item;
			return;
		}

		LinkNode curLink = new LinkNode();
		curLink.prevLink = m_lastLink;
		m_lastLink.nextLink = curLink;
		m_lastLink.linkElement = item;
		m_lastLink = curLink;
	}
	
	public void clear() {
		m_firstLink.nextLink = m_lastLink;
		m_lastLink.prevLink = m_firstLink;
		m_firstLink.linkElement = null;
	}
	
	public void popFront() {
		if (m_firstLink.nextLink == m_lastLink) {
			m_firstLink.linkElement = null;
			return;
		}
		
		m_firstLink = m_firstLink.nextLink;
		m_firstLink.prevLink = null;
	}
	
	public void popBack() {
		if (m_firstLink.nextLink == m_lastLink) {
			m_firstLink.linkElement = null;
			return;
		}
		
		m_lastLink.prevLink = m_lastLink.prevLink.prevLink;
		m_lastLink.prevLink.nextLink = m_lastLink;
	}
	
	public void erase(int idx) {
		if (idx == 0) {
			popFront();
			return;
		}
		
		int i = 1;
		for (LinkNode itr = m_firstLink.nextLink;
				itr != m_lastLink; itr = itr.nextLink)
		{
			if (i++ != idx)
				continue;
			
			itr.prevLink.nextLink = itr.nextLink;
			itr.nextLink.prevLink = itr.prevLink;
			return;
		}
		
		// Ungültiger Index -> popBack
		popBack();
	}

	public void removeAll(T item) {
		for (LinkNode itr = m_firstLink; itr != m_lastLink; itr = itr.nextLink) {
			if (itr.linkElement != item)
				continue;

			else if (itr == begin())
				popFront();

			else if (itr == end())
				popBack();

			else {
				itr.prevLink.nextLink = itr.nextLink;
				itr.nextLink.prevLink = itr.prevLink;
			}
		}
	}

	public T[] asArray(Class<T> elementType) {
		@SuppressWarnings("unchecked")
		T[] array = (T[]) Array.newInstance(elementType, getSize());

		int i = 0;
		for (LinkNode itr = begin(); itr != end(); itr = itr.nextLink)
			array[i++] = itr.linkElement;
		return array;
	}
}
