package stockgame;
import java.util.Random;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * File for the RandomStockPriceProvider, uses some random stock prices
 *
 */

public class RandomStockPriceProvider extends StockPriceProvider {
	private Random m_random = new Random();

	public RandomStockPriceProvider(SharePool pool) {
		super(pool);
	}

	@Override
	protected void updateShareRate(Share share) {
		long diff = m_random.nextInt(5) - 2;
		long course = share.getCourse() + diff;
		if (course < 1)
			course = 1;
		share.setCourse(course);
	}
}
