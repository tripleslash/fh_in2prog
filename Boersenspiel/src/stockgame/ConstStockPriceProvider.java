package stockgame;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * File for the ConstStockPriceProvider
 *
 */

public class ConstStockPriceProvider extends StockPriceProvider {

	public ConstStockPriceProvider(SharePool pool) {
		super(pool);
	}

	@Override
	protected void updateShareRate(Share share) {
		// Konstanter Provider - tut nichts
	}
}
