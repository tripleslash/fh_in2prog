package stockgame.automation;

import stockgame.IStockGameClient;
import stockgame.Share;
import stockgame.ShareItem;
import stockgame.SharePool;
import utils.GlobalTimer;

import java.util.TimerTask;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * The StockGameWorkerAgent
 *
 */

public class StockGameWorkerAgent implements IStockGameAgent {
	private IStockGameClient m_client = null;
	private SharePool m_sharePool = null;

	@Override
	public void setClient(IStockGameClient client) {
		m_client = client;
	}

	@Override
	public IStockGameClient getClient() {
		return m_client;
	}

	@Override
	public void setSharePool(SharePool pool) {
		m_sharePool = pool;
	}

	@Override
	public SharePool getSharePool() {
		return m_sharePool;
	}

	public void beginWork() {
		GlobalTimer.getInstance()
		.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				try {
					StockGameWorkerAgent.this.work();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, 2000, 2000);
	}

	private void work() throws Exception {
		for (Share share : m_sharePool.getAllShares()) {
			long clientMoney = m_client.getMoney();
			long shareCourse = share.getCourse();
			Share.ChangeState state = share.getChangeState();

			if (state == Share.ChangeState.Increasing)
				m_client.buyShares(share, (clientMoney / 6) / shareCourse);
		}

		for (ShareItem shareItem : m_client.getHeldShareItems()) {
			if (shareItem.calcWinLossDelta() > 3)
				m_client.sellShares(shareItem.getShare(), shareItem.getCount());
		}
	}
}
