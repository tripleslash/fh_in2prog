package stockgame.automation;

import stockgame.IStockGameClient;
import stockgame.SharePool;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * Interface for the StockGameAgent, override the methods to use it
 *
 */

public interface IStockGameAgent {
	// Dem Agent einen neuen Auftraggeber zuweisen
	void setClient(IStockGameClient client);

	// Gibt den aktuellen Auftraggeber des Agenten zur�ck
	IStockGameClient getClient();

	// Weist dem Agent den aktuelle Aktienpool zu
	void setSharePool(SharePool pool);

	// Gibt den aktuell verwendeten Aktienpool des Agenten zur�ck
	SharePool getSharePool();
}
