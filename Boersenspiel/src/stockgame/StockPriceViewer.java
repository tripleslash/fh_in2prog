package stockgame;

import utils.GlobalTimer;
import java.util.TimerTask;
import javax.swing.JFrame;
import javax.swing.JLabel;
	
/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * File for the jFrame-Window
 *
 */

public class StockPriceViewer extends JFrame {
	private static final int TICK_PERIOD = 1000;
	private JLabel m_stockLabel = null;
	private SharePool m_sharePool = null;
	private PlayerAccountPool m_playerPool = null;

	private class TickerTask extends TimerTask {
		public void run() {
			String output = createText();
			m_stockLabel.setText(output);
			m_stockLabel.repaint();
		}

		private String createText() {
			String output = "<html><body>";
			output += "<b>Aktienticker:</b><br>";
			for (Share s : m_sharePool.getAllShares())
				output += " - " + s.toString() + "<br>";

			output += "<br><b>Aktuelle Spieler:</b><br>";
			for (Player p : m_playerPool.getAllPlayers()) {
				output += " - " + p.toString() + "<br>";
				/*output += "   Gehaltene Aktienpakete:<br>";
				for (ShareItem s : p.getHeldShareItems())
					output += "     - " + s.toString() + "<br>";*/
			}

			output += "</body></html>";
			return output;
		}
	}

	public StockPriceViewer(PlayerAccountPool players, SharePool shares) {
		m_sharePool = shares;
		m_playerPool = players;
		m_stockLabel = new JLabel("coming soon ...");

		add(m_stockLabel);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(600, 400);
		setVisible(true);
	}

	public void start() {
		GlobalTimer.getInstance().scheduleAtFixedRate(
			new TickerTask(), 0, TICK_PERIOD);
	}
}
