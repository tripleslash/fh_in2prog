package stockgame;

import stockgame.exceptions.*;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * Interface of the StockGameClient, override methods to use it
 *
 */

public interface IStockGameClient {
	// Gibt alle Aktienpakete des Clients zur�ck
	ShareItem[] getHeldShareItems();

	// Guthaben des Clienten zur�ckgeben
	long getMoney();

	// Aktienpaket mit n-Aktien kaufen
	void buyShares(Share s, long amount) throws NotEnoughMoneyException;

	// Bestimmte Aktien aus den Paketen verkaufen
	void sellShares(Share s, long amount) throws NotEnoughSharesException;
}
