package stockgame;

import java.util.LinkedList;
import java.util.List;
import stockgame.exceptions.PlayerNotFoundException;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * This is the PlayAccountPool, some Players are in there
 *
 */

public class PlayerAccountPool {
	private List<Player> m_players = new LinkedList<Player>();

	// Neuen Spieler in den Pool legen
	public void addPlayer(Player p) {
		m_players.add(p);
	}

	// Spieler vom Pool entfernen
	public void removePlayer(Player p) {
		m_players.remove(p);
	}

	// Spieler im Pool suchen, PlayerNotFoundException falls nicht gefunden
	public Player queryPlayer(String playerName) throws PlayerNotFoundException {
		Player plr = queryPlayerNoThrow(playerName);
		if (plr == null)
			throw new PlayerNotFoundException(playerName);
		return plr;
	}

	// Spieler im Pool suchen, null falls nicht gefunden
	public Player queryPlayerNoThrow(String playerName) {
		int hashCode = playerName.hashCode();
		for (Player plr : m_players) {
			if (plr.getNameHash() == hashCode)
				return plr;
		}

		return null;
	}

	// Spielerpool leer?
	public boolean isEmpty() {
		return m_players.isEmpty();
	}

	// Spieler vorhanden?
	public boolean isPlayerListed(String playerName) {
		return queryPlayerNoThrow(playerName) != null;
	}

	public Player[] getAllPlayers() {
		return m_players.toArray(new Player[m_players.size()]);
	}
}
