package stockgame;

import stockgame.exceptions.NotEnoughMoneyException;
import java.lang.IllegalArgumentException;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * File for the CashAccount of the Player
 *
 */

// Verm�gen der Spieler
public class CashAccount extends Asset {
	private long m_money;

	public CashAccount(String name, long money) {
		super(name);
		m_money = money;
	}

	// Gibt das aktuelle Verm�gen an
	public long getValue() {
		return m_money;
	}

	// F�gt Geld zum Konto hinzu
	public void addMoney(long money) {
		if (money < 0)
			throw new IllegalArgumentException("addMoney wurde mit negativem Geldbetrag aufgerufen!");
		m_money += money;
	}

	// Entfernt Geld vom Konto
	public void removeMoney(long money) throws NotEnoughMoneyException {
		if (money < 0)
			throw new IllegalArgumentException("removeMoney wurde mit negativem Geldbetrag aufgerufen!");

		if (m_money < money)
			throw new NotEnoughMoneyException();

		m_money -= money;
	}

	public String toString() {
		return "Geldkonto \"" + getName() + "\" - Gesamtwert: " + getValue();
	}
}
