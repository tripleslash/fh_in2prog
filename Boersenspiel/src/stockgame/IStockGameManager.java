package stockgame;

import java.io.IOException;
import stockgame.automation.IStockGameAgent;
import stockgame.exceptions.*;
import utils.MimeType;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * Interface for the StockGameManager, override methods to use it
 *
 */

public interface IStockGameManager {
	// Aktienpool zur�ckgeben
	SharePool getSharePool();

	// Spielerpool zur�ckgeben
	PlayerAccountPool getPlayerPool();

	// Neuen Spieler anlegen
	Player createPlayer(String name, long initialMoney) throws PlayerDuplicateException;

	// Neue Aktie anlegen
	Share createShare(String name, long initialCourse) throws ShareDuplicateException;

	// Neuen Agent anlegen
	IStockGameAgent createAgentForPlayer(String playerName);

	// Spieler und Aktie finden und vom Spieler Aktien verkaufen
	void sellShares(String playerName, String shareName, int amount) throws NotEnoughSharesException;

	// Spieler und Aktie finden und vom Spieler Aktien kaufen
	void buyShares(String playerName, String shareName, int amount) throws NotEnoughMoneyException;

	// Geld des Spielers abfragen
	long getMoney(String playerName);

	// Guthaben des Spielers in Aktien abfragen
	long getShareValue(String playerName);

	// Gesamtguthaben des Spielers abfragen
	long getTotalValue(String playerName);

	// Gibt die Transaktionen eines Spielers als Zeichenkette zur�ck
	String getTransactionsForPlayer(String playerName, boolean sortTransactions);

	// Gibt bestimmte Transaktionen eines Spielers als Zeichenkette zur�ck
	String getTransactionsByType(String playerName, String shareName);

	// Schreibt die Transaktionen im angegebenen Format in eine Datei
	void writeTransactionsForPlayer(String playerName, String fileName, MimeType mimeType) throws IOException;

	// Gesamtgewinn/Verlust in Aktien des Spielers abfragen
	long calcTotalWinLossDelta(String playerName);

	// Gesamtgewinn/Verlust des Spielers f�r eine spezielle Aktie
	long calcWinLossDeltaForShare(String playerName, String shareName);

	// Gibt alle Stats in textueller Form zur�ck, null falls nicht gefunden
	String getStats();
}

