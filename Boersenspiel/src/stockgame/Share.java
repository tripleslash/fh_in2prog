package stockgame;

import java.util.logging.Logger;
import java.text.NumberFormat;
import stockgame.localisation.LocalisationManager;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * The file for all Shares
 *
 */

// Eine einzelne Aktie
public class Share implements Comparable<Share> {
	public static enum ChangeState {
		Increasing,
		Decreasing
	};

	// Name der Aktie und aktueller Kurs
	private String m_name;
	private long m_course;
	private ChangeState m_changeState;

	// Fuerr schnelles suchen in einer Collection
	private int m_nameHash;

	private static Logger s_logger = Logger.getLogger(Share.class.getName());

	public Share(String name, long course) {
		m_name = name;
		m_course = course;
		m_nameHash = name.hashCode();
		s_logger.fine("Neue Aktie angelegt. Name: \"" + name + "\", Kurs: " + course);
	}

	public String getName() {
		return m_name;
	}

	public int getNameHash() {
		return m_nameHash;
	}

	public long getCourse() {
		return m_course;
	}

	public String getLocalisedCourse() {
		LocalisationManager mgr = LocalisationManager.getInstance();
		NumberFormat nf = NumberFormat.getCurrencyInstance(mgr.getLocale());
		return nf.format(((float)getCourse()) / 100.0f);
	}

	public ChangeState getChangeState() {
		return m_changeState;
	}

	public void setCourse(long course) {
		if (m_course == course)
			return;

		if (course > m_course)
			m_changeState = ChangeState.Increasing;
		else
			m_changeState = ChangeState.Decreasing;

		s_logger.fine("Kursaenderung von \"" + getName() + "\" von " + m_course + " auf " + course);
		m_course = course;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Share))
			return false;
		return compareTo((Share) o) == 0;
	}

	@Override
	public int compareTo(Share other) {
		int nameCompare = getName().compareTo(other.getName());
		if (nameCompare != 0)
			return nameCompare;

		Long course = new Long(getCourse());
		return course.compareTo(new Long(other.getCourse()));
	}

	@Override
	public String toString() {
		LocalisationManager mgr = LocalisationManager.getInstance();
		return mgr.getString("share") + " \"" + getName() + "\", "
				+ mgr.getString("course") + ": " + getLocalisedCourse();
	}
}