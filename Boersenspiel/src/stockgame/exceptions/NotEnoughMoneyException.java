package stockgame.exceptions;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * NotEnoughMoneyException
 *
 */

public class NotEnoughMoneyException extends Exception {
	public NotEnoughMoneyException() {
		super("Nicht genug Geld vorhanden!");
	}
}
