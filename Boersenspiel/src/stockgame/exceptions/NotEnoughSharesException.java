package stockgame.exceptions;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * NotEnoughSharesException
 *
 */

public class NotEnoughSharesException extends Exception {
	public NotEnoughSharesException() {
		super("Nicht genug Aktien vorhanden!");
	}
}
