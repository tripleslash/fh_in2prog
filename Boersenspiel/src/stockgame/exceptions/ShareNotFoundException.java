package stockgame.exceptions;

import utils.ItemNotFoundException;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * ShareNotFoundException
 *
 */

public class ShareNotFoundException extends ItemNotFoundException {
	public ShareNotFoundException(String name) {
		super("Aktie \"" + name + "\" nicht gefunden!");
	}
}
