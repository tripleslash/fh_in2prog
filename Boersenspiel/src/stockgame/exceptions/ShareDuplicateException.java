package stockgame.exceptions;

import utils.ItemDuplicateException;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * ShareDuplicateException
 *
 */

public class ShareDuplicateException extends ItemDuplicateException {
	public ShareDuplicateException(String name) {
		super("Aktie \"" + name + "\" dupliziert!");
	}
}
