package stockgame.exceptions;

import utils.ItemDuplicateException;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * PlayerDuplicateException
 *
 */

public class PlayerDuplicateException extends ItemDuplicateException {
	public PlayerDuplicateException(String name) {
		super("Spieler \"" + name + "\" dupliziert!");
	}
}
