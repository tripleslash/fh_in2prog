package stockgame.exceptions;

import utils.ItemNotFoundException;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * PlayerNotFoundException
 *
 */

public class PlayerNotFoundException extends ItemNotFoundException {
	public PlayerNotFoundException(String name) {
		super("Spieler \"" + name + "\" nicht gefunden!");
	}
}
