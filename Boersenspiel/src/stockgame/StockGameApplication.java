package stockgame;

import java.lang.reflect.Proxy;

import utils.commands.UniversalCommandProcessor;
import utils.logging.InvocationLogger;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;
import stockgame.*;
import utils.*;
import stockgame.StockGameController;
import stockgame.exceptions.NotEnoughMoneyException;
import stockgame.exceptions.NotEnoughSharesException;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 *          Main(), use it to run the progam
 *
 */

public class StockGameApplication extends Application {

	public static void main(String[] args) {

		Application.launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {

		final IStockGameManager m_gameManager = new StockGameManager();

		// Logging-Proxy erzeugen und an den Spielmanager anheften
		InvocationLogger logger = new InvocationLogger(m_gameManager);
		IStockGameManager gameMgrProxy = (IStockGameManager) Proxy
				.newProxyInstance(m_gameManager.getClass().getClassLoader(),
						new Class[] { IStockGameManager.class }, logger);

		// Aktienpool abfragen
		SharePool sharePool = gameMgrProxy.getSharePool();

		// Aktien erzeugen
		Share bmw = new Share("BMW", 5000);
		Share apple = new Share("Apple", 5000);
		Share microsoft = new Share("Microsoft", 5000);
		Share infineon = new Share("Infineon", 5000);
		Share google = new Share("Google", 5000);

		// Aktien in den Pool legen
		sharePool.addShare(bmw);
		sharePool.addShare(apple);
		sharePool.addShare(microsoft);
		sharePool.addShare(infineon);
		sharePool.addShare(google);

		// Spielerpool abfragen
		PlayerAccountPool playerPool = gameMgrProxy.getPlayerPool();

		Player andreas = new Player("Andreas", 800000);
		Player michael = new Player("Michael", 1000000);

		playerPool.addPlayer(andreas);
		playerPool.addPlayer(michael);

		// Neuen Aktienkurs-Provider erstellen und starten
		HistoricalStockPriceProvider prov = new HistoricalStockPriceProvider(
				sharePool);
		prov.loadStockPriceData(bmw, "bmw.csv");
		prov.loadStockPriceData(apple, "aapl.csv");
		prov.loadStockPriceData(microsoft, "msft.csv");
		prov.loadStockPriceData(infineon, "ifnny.csv");
		prov.loadStockPriceData(google, "googl.csv");
		prov.start();

		// F�r die restlichen, nicht auf historischen Kursen basierende Aktien
		// einen Random-Provider
		new RandomStockPriceProvider(sharePool).start();

		// Kommandoprozessor an den Spielmanager anf�gen
		UniversalCommandProcessor processor = new UniversalCommandProcessor();
		processor.attachToTarget(m_gameManager, logger);

		(new Thread() {
			@Override
			public void run() {
				while (true)
					processor.pollNext();
			}
		}).start();

		/**
		 * 
		 * GUI Version: 0.1 Verwendetes Pane: BorderPane Größe: 850x480, nicht
		 * resizebar
		 * 
		 * Zur Positionierung stehen zur Verfügung: setTop, setBottom,
		 * setCenter, setLeft, setRight Die Insets(Innenabstand) setzt sich
		 * zusammen aus: Oben, Rechts, Unten, Links
		 * 
		 */

		// Das verwendete Pane
		BorderPane pane = new BorderPane();

		// Die Scene
		Scene scene = new Scene(pane);

		// Das Fenster allgemein
		stage.setTitle("Boersenspiel");
		stage.setX(100);
		stage.setY(100);
		stage.setHeight(480);
		stage.setWidth(850);
		stage.setResizable(false);
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent arg0) {
				System.exit(0);
			}
		});
		stage.setScene(scene);
		stage.show();

		// Box für die Eingabezeilen und Label
		VBox eingabebox = new VBox(2.0);
		Label label = new Label();
		label.setText("1. Eingabezeile für Namen, 2. Eingabezeile für Aktie, 3. Eingabezeile für Geldbetrag/Stückzahl");
		TextField name = new TextField();
		TextField share = new TextField();
		TextField amge = new TextField();
		name.setMaxHeight(20);
		name.setMaxWidth(500);
		share.setMaxHeight(20);
		share.setMaxWidth(500);
		amge.setMaxHeight(20);
		amge.setMaxWidth(500);
		eingabebox.getChildren().add(name);
		eingabebox.getChildren().add(share);
		eingabebox.getChildren().add(amge);
		eingabebox.getChildren().add(label);
		eingabebox.setAlignment(Pos.BOTTOM_LEFT);
		pane.setBottom(eingabebox);
		pane.setMargin(eingabebox, new Insets(10, 10, 10, 10));
		pane.setMargin(label, new Insets(10, 10, 10, 10));

		// Textfeld für Ausgaben
		TextArea centerText = new TextArea();
		pane.setCenter(centerText);
		centerText.setMaxHeight(250);
		centerText.setMaxWidth(400);
		pane.setMargin(centerText, new Insets(10, 10, 10, 25));

		final ContextMenu contextMenu = new ContextMenu();
		MenuItem cut = new MenuItem("Ausschneiden");
		MenuItem copy = new MenuItem("Kopieren");
		MenuItem paste = new MenuItem("Einfuegen");
		contextMenu.getItems().addAll(cut, copy, paste);
		cut.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.out.println("Aktion ausgefuehrt");
			}
		});

		centerText.setContextMenu(contextMenu);

		// Menü mit einigen Punkten
		MenuBar menuBar = new MenuBar();
		Menu datei = new Menu("Datei");
		Menu bearbeiten = new Menu("Bearbeiten");
		Menu ansicht = new Menu("Ansicht");
		Menu exit = new Menu("Exit");
		exit.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent ae) {
				System.exit(0);
			}
		});
		menuBar.getMenus().addAll(datei, bearbeiten, ansicht, exit);
		pane.setTop(menuBar);
		pane.setMargin(menuBar, new Insets(10, 10, 10, 10));

		// Die Button-Box
		VBox buttonBox = new VBox(2.0);
		Button spieler = new Button("Spieler anlegen");
		spieler.setTooltip(new Tooltip(
				"Über diesen Button wird ein neuer Spieler angelegt"));
		spieler.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent ae) {

				String spieler;
				Long money;
				spieler = name.getText();
				money = Long.parseLong(amge.getText());
				m_gameManager.createPlayer(spieler, money);

			}
		});
		Button aktieanlegen = new Button("Aktie anlegen");
		aktieanlegen.setTooltip(new Tooltip(
				"Über diesen Button wird eine neue Aktie angelegt"));
		aktieanlegen.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent ae) {

				String aktie;
				Long money;
				aktie = share.getText();
				money = Long.parseLong(amge.getText());
				m_gameManager.createShare(aktie, money);

			}
		});
		Button transaktion = new Button("Transaktionen anzeigen");
		transaktion.setTooltip(new Tooltip(
				"Zeigt die Transaktionen eines bestimmten Spieler an"));
		transaktion.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent ae) {

				String spieler;
				String aktie;
				spieler = name.getText();
				aktie = share.getText();
				centerText.setText(m_gameManager.getTransactionsByType(spieler,
						aktie));

			}
		});
		Button statistik = new Button("Statistik anzeigen");
		statistik.setTooltip(new Tooltip(
				"Zeigt die Statistiken aller Spieler und Aktien an"));
		statistik.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent ae) {

				centerText.setText(m_gameManager.getStats());

			}
		});
		Button agent = new Button("Spieleragent anmelden");
		agent.setTooltip(new Tooltip(
				"Registriert einen Agenten für den Spieler"));
		agent.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {

				String spieler;
				spieler = name.getText();
				m_gameManager.createAgentForPlayer(spieler);

			}
		});
		spieler.setMinWidth(180);
		aktieanlegen.setMinWidth(180);
		transaktion.setMinWidth(180);
		statistik.setMinWidth(180);
		agent.setMinWidth(180);
		;
		buttonBox.getChildren().add(spieler);
		buttonBox.getChildren().add(aktieanlegen);
		buttonBox.getChildren().add(agent);
		buttonBox.getChildren().add(transaktion);
		buttonBox.getChildren().add(statistik);
		buttonBox.setAlignment(Pos.BOTTOM_RIGHT);
		pane.setRight(buttonBox);
		pane.setMargin(buttonBox, new Insets(10, 10, 10, 10));

		// Der Graph, gezeichnet von JavaFX mit einigen Angaben
		final NumberAxis xAxis = new NumberAxis();
		final NumberAxis yAxis = new NumberAxis();
		final LineChart<Number, Number> lineChart = new LineChart<Number, Number>(
				xAxis, yAxis);
		lineChart.setTitle("Boersenspiel\nHochschule Augsburg");
		XYChart.Series series = new XYChart.Series();
		series.setName("DAX - 1 Jahr");
		series.getData().add(new XYChart.Data(1, 23));
		series.getData().add(new XYChart.Data(2, 14));
		series.getData().add(new XYChart.Data(3, 15));
		series.getData().add(new XYChart.Data(4, 24));
		series.getData().add(new XYChart.Data(5, 34));
		series.getData().add(new XYChart.Data(6, 36));
		series.getData().add(new XYChart.Data(7, 22));
		series.getData().add(new XYChart.Data(8, 30));
		series.getData().add(new XYChart.Data(9, 32));
		series.getData().add(new XYChart.Data(10, 34));
		pane.setLeft(lineChart);
		lineChart.getData().add(series);
		lineChart.setMaxHeight(350);
		lineChart.setMaxWidth(250);
		pane.setMargin(lineChart, new Insets(10, 10, 10, 10));

	}

}
