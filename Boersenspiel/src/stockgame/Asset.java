package stockgame;


/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * File for the abstract class Asset
 *
 */
public abstract class Asset {
	private String m_assetName;

	// F�r schnelles suchen in einer Collection
	private int m_nameHash;

	protected Asset(String name) {
		m_assetName = name;
		m_nameHash = name.hashCode();
	}

	public String getName() {
		return m_assetName;
	}

	public int getNameHash() {
		return m_nameHash;
	}

	public abstract long getValue();
	public abstract String toString();
}
