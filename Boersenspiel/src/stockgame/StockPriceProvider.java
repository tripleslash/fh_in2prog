package stockgame;

import utils.GlobalTimer;
import java.util.TimerTask;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * File for the StockPriceProvider
 *
 */

public abstract class StockPriceProvider {
	private SharePool m_sharePool = null;

	public StockPriceProvider(SharePool pool) {
		m_sharePool = pool;
	}

	public void setSharePool(SharePool pool) {
		m_sharePool = pool;
	}

	public SharePool getSharePool() {
		return m_sharePool;
	}

	public void start() {
		GlobalTimer.getInstance()
		.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				updateShareRates();
			}
		}, 2000, 2000);
	}

	// Alle Shares updaten
	// Da der TickerTask asynchron die Shares durchgeht, muss man hier synchronisieren
	protected void updateShareRates() {
		for (Share share : m_sharePool.getAllShares())
			updateShareRate(share);
	}

	protected abstract void updateShareRate(Share share);
}
