package stockgame;

import java.util.List;
import java.util.LinkedList;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * The ShareDepositAccount, holding all Shares
 *
 */

// Gesamtes Aktiendepot
public class ShareDepositAccount extends Asset {
	private List<ShareItem> m_shareItems = new LinkedList<ShareItem>();

	public ShareDepositAccount(String name) {
		super(name);
	}

	// Gesamtwert des Aktiendepots abfragen
	public long getValue() {
		long value = 0;
		for (ShareItem curItem : m_shareItems)
			value += curItem.getValue();

		return value;
	}

	// Neues Aktienpaket hinzuf�gen
	public void addShareItem(ShareItem s) {
		m_shareItems.add(s);
	}

	// Aktienpaket entfernen
	public void removeShareItem(ShareItem s) {
		m_shareItems.remove(s);
	}

	// Gibt zur�ck wie viel Gewinn oder Verlust gemacht wurde (mit allen Paketen)
	public long calcTotalWinLossDelta() {
		long winLossDelta = 0;
		for (ShareItem curItem : m_shareItems)
			winLossDelta += curItem.calcWinLossDelta();

		return winLossDelta;
	}

	// Gibt zur�ck wie viel Gewinn oder Verlust mit einer speziellen Aktie gemacht wurde
	public long calcWinLossDeltaForShare(Share s) {
		// F�r alle Aktienpakete dieses Typs aufsummieren
		long winLossDelta = 0;
		for (ShareItem shareItem : itemsByType(s))
			winLossDelta += shareItem.calcWinLossDelta();

		return winLossDelta;
	}

	// Alle Aktienpakete zur�ckgeben
	public ShareItem[] getHeldShareItems() {
		return m_shareItems.toArray(new ShareItem[m_shareItems.size()]);
	}

	// Alle Aktienpakete einer bestimmten Aktie abfragen
	public ShareItem[] itemsByType(Share s) {
		LinkedList<ShareItem> shares = new LinkedList<ShareItem>();

		for (ShareItem curItem : m_shareItems) {
			if (curItem.getShare() == s)
				shares.add(curItem);
		}

		return shares.toArray(new ShareItem[shares.size()]);
	}

	@Override
	public String toString() {
		return "Aktienkonto \"" + getName() + "\" - Gesamtwert: " + getValue();
	}
}
