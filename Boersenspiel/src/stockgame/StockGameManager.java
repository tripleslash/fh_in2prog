package stockgame;

import stockgame.automation.*;
import stockgame.exceptions.*;
import stockgame.transaction.*;
import utils.commands.AsCommand;
import utils.MimeType;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * StockGameManager gives you the ability to use some commands in the program
 *
 */

public final class StockGameManager implements IStockGameManager {
	private SharePool m_sharePool = new SharePool();
	private PlayerAccountPool m_playerPool = new PlayerAccountPool();

	@Override
	public SharePool getSharePool() {
		return m_sharePool;
	}

	@Override
	public PlayerAccountPool getPlayerPool() {
		return m_playerPool;
	}

	@Override
	@AsCommand(name="createPlayer", description="Erstellt einen neuen Spieler. Parameter: name, initialMoney")
	public Player createPlayer(String name, long initialMoney) throws PlayerDuplicateException {
		// Falls es schon einen Spieler mit diesem Namen gibt -> PlayerDuplicateException
		if (m_playerPool.isPlayerListed(name))
			throw new PlayerDuplicateException(name);

		Player plr = new Player(name, initialMoney);
		m_playerPool.addPlayer(plr);
		return plr;
	}

	@Override
	@AsCommand(name="createShare", description="Erstellt eine neue Aktie. Parameter: name, initialCourse")
	public Share createShare(String name, long initialCourse) throws ShareDuplicateException {
		// Falls es schon eine Aktie mit diesem Namen gibt -> ShareDuplicateException
		if (m_sharePool.isShareListed(name))
			throw new ShareDuplicateException(name);

		Share s = new Share(name, initialCourse);
		m_sharePool.addShare(s);
		return s;
	}

	@Override
	@AsCommand(name="createAgentForPlayer", description="Erstellt einen neuen Aktienagent f�r den angegebenen Spieler. Parameter: playerName")
	public IStockGameAgent createAgentForPlayer(String playerName) {
		StockGameWorkerAgent workerAgent = new StockGameWorkerAgent();
		workerAgent.setClient(m_playerPool.queryPlayer(playerName));
		workerAgent.setSharePool(m_sharePool);
		workerAgent.beginWork();
		return workerAgent;
	}

	@Override
	@AsCommand(name="playerSellShares", description="Verkauft Aktien des angegebenen Spielers. Parameter: playerName, shareName, amount")
	public void sellShares(String playerName, String shareName, int amount) throws NotEnoughSharesException {
		Share s = m_sharePool.queryShare(shareName);
		m_playerPool.queryPlayer(playerName).sellShares(s, amount);
	}

	@Override
	@AsCommand(name="playerBuyShares", description="Kauft Aktien unter dem angegebenen Spieler. Parameter: playerName, shareName, amount")
	public void buyShares(String playerName, String shareName, int amount) throws NotEnoughMoneyException {
		Share s = m_sharePool.queryShare(shareName);
		m_playerPool.queryPlayer(playerName).buyShares(s, amount);
	}

	@Override
	@AsCommand(name="playerGetMoney", description="Gibt das aktuelle Geldguthaben des Spielers zur�ck. Parameter: playerName")
	public long getMoney(String playerName) {
		return m_playerPool.queryPlayer(playerName).getMoney();
	}

	@Override
	@AsCommand(name="playerGetShareValue", description="Gibt das aktuelle Verm�gen (in Aktien) des Spielers zur�ck. Parameter: playerName")
	public long getShareValue(String playerName) {
		return m_playerPool.queryPlayer(playerName).getShareValue();
	}

	@Override
	@AsCommand(name="playerGetValue", description="Gibt das aktuelle Gesamtverm�gen (Aktien, Geld...) des Spielers zur�ck. Parameter: playerName")
	public long getTotalValue(String playerName) {
		return m_playerPool.queryPlayer(playerName).getValue();
	}

	@Override
	@AsCommand(name="playerGetTransactions", description="Gibt die Transaktionen eines Spielers (get�tigte K�ufe/Verk�ufe...) zur�ck. Parameter: playerName, sortTransactions")
	public String getTransactionsForPlayer(String playerName, boolean sortTransactions) {
		StringBuilder result = new StringBuilder();
		result.append("Transaktionen f�r \"" + playerName + "\":\n");

		Player player = m_playerPool.queryPlayer(playerName);
		List<SimpleTransaction> transactions = new ArrayList<
			SimpleTransaction>(Arrays.asList(player.getTransactions()));

		transactions.stream().sorted().forEach(i ->
			result.append(" - " + i.toString() + "\n"));
		return result.toString();
	}

	@Override
	@AsCommand(name="playerGetTransactionsByType", description="Gibt bestimmte Transaktionen eines Spielers (get�tigte K�ufe/Verk�ufe...) einer bestimmten Aktie zur�ck. Parameter: playerName, shareName")
	public String getTransactionsByType(String playerName, String shareName) {
		StringBuilder result = new StringBuilder();
		result.append("\"" + shareName + "\"-Transaktionen f�r \"" + playerName + "\":\n");

		Player player = m_playerPool.queryPlayer(playerName);
		Share share = m_sharePool.queryShare(shareName);
		SimpleTransaction[] transactions = player.getTransactions();

		for (SimpleTransaction trans : transactions) {
			if (!(trans instanceof ShareTransaction))
				continue;

			if (((ShareTransaction)trans).getShare() == share)
				result.append(" - " + trans.toString() + "\n");
		}
		return result.toString();
	}

	@Override
	@AsCommand(name="writeTransactionsForPlayer", description="Schreibt die Transaktionen im angegebenen Format in eine Datei. Wenn fileName = '|' erfolgt die Ausgabe auf der Konsole. Parameter: playerName, fileName, mimeType")
	public void writeTransactionsForPlayer(String playerName, String fileName, MimeType mimeType) throws IOException {
		PrintStream printStream = System.out;
		if (!fileName.equals("|")) { // Konsolenausgabe wenn fileName = '|'
			File file = new File(fileName);
			if (!file.exists())
				file.createNewFile();

			FileOutputStream fileStream = new FileOutputStream(file, false);
			printStream = new PrintStream(fileStream);
		}

		Player player = m_playerPool.queryPlayer(playerName);
		player.writeTransactions(printStream, mimeType);
	}

	@Override
	@AsCommand(name="playerCalcTotalWinLossDelta", description="Gibt zur�ck wie viel Gewinn oder Verlust mit allen Aktien gemacht wurde. Parameter: playerName")
	public long calcTotalWinLossDelta(String playerName) {
		return m_playerPool.queryPlayer(playerName).calcTotalWinLossDelta();
	}

	@Override
	@AsCommand(name="playerCalcWinLossDeltaForShare", description="Gibt zur�ck wie viel Gewinn oder Verlust mit einer Aktie gemacht wurde. Parameter: playerName, shareName")
	public long calcWinLossDeltaForShare(String playerName, String shareName) {
		Share s = m_sharePool.queryShare(shareName);
		return m_playerPool.queryPlayer(playerName).calcWinLossDeltaForShare(s);
	}

	@Override
	@AsCommand(name="getStats", description="Gibt eine aktuelle Statistik der Spieler und Aktienwerte zur�ck. Parameter: keine")
	public String getStats() {
		StringBuilder stats = new StringBuilder();
		if (!m_playerPool.isEmpty()) {
			stats.append("Eingetragene Spieler:\n");
			for (Player plr : m_playerPool.getAllPlayers())
				stats.append("  " + plr.toString() + "\n");
		}

		if (!m_sharePool.isEmpty()) {
			stats.append("Eingetragene Aktien:\n");
			for (Share share : m_sharePool.getAllShares())
				stats.append("  " + share.toString() + "\n");
		}

		return stats.toString();
	}
}

