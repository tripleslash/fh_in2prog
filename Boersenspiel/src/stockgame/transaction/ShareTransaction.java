package stockgame.transaction;

import stockgame.Share;
import stockgame.localisation.LocalisationManager;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * This is the File for the ShareTransaction
 *
 */

public class ShareTransaction extends SimpleTransaction {
	public static enum Type {
		BuyShares,
		SellShares
	}

	private Type m_transactionType;
	private long m_transactionAmount;
	private Share m_shareObject;

	public ShareTransaction(Type type, Share share, long amount) {
		m_transactionType = type;
		m_transactionAmount = amount;
		m_shareObject = share;
	}

	public Type getType() {
		return m_transactionType;
	}

	public long getAmount() {
		return m_transactionAmount;
	}

	public Share getShare() {
		return m_shareObject;
	}

	@Override
	public int compareTo(SimpleTransaction o) {
		if (!(o instanceof ShareTransaction))
			return -1; // Keine Aktientransaktion -> kann nicht gleich sein

		// Aktientyp vergleichen
		ShareTransaction other = (ShareTransaction) o;
		int shareCompare = getShare().compareTo(other.getShare());
		if (shareCompare != 0)
			return shareCompare;

		// Transaktionstyp vergleichen
		int typeCompare = getType().compareTo(other.getType());
		if (typeCompare != 0)
			return typeCompare;

		// Transaktions-St�ckzahl vergleichen
		Long amount = new Long(getAmount());
		int amountCompare = amount.compareTo(new Long(other.getAmount()));
		if (amountCompare != 0)
			return amountCompare;

		// Fall-back zum Standardvergleich
		return super.compareTo(o);
	}

	@Override
	public String getTransaction() {
		StringBuilder sb = new StringBuilder();
		LocalisationManager mgr = LocalisationManager.getInstance();

		switch (m_transactionType) {
		case BuyShares:
			sb.append(mgr.getString("purchaseOf") + getShare());
			break;

		case SellShares:
			sb.append(mgr.getString("saleOf") + getShare());
			break;
		}

		sb.append(". " + mgr.getString("amount") + ": " + getAmount());
		return sb.toString();
	}
}
