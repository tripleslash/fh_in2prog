package stockgame.transaction;

import java.sql.Timestamp;
import utils.MimeType;
import java.text.SimpleDateFormat;
import stockgame.localisation.LocalisationManager;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * This is the file for the SimpleTransaction, implements Comparable
 *
 */

public abstract class SimpleTransaction implements Comparable<SimpleTransaction> {
	private Timestamp m_timestamp = new Timestamp(System.currentTimeMillis());

	public Timestamp getTimestamp() {
		return m_timestamp;
	}

	@Override
	public int compareTo(SimpleTransaction o) {
		return getTimestamp().compareTo(o.getTimestamp());
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof SimpleTransaction))
			return false;
		return compareTo((SimpleTransaction) o) == 0;
	}

	public String getTransaction() {
		LocalisationManager mgr = LocalisationManager.getInstance();
		return mgr.getString("unknownTransaction");
	}

	public String getLocalisedTimestamp() {
		LocalisationManager mgr = LocalisationManager.getInstance();
		return mgr.convertDate(getTimestamp());
	}

	@Override
	public String toString() {
		return getLocalisedTimestamp() + " - " + getTransaction();
	}

	public String toStringMime(MimeType type) {
		switch (type) {
		case TextHtml:
			return "<td style=\"padding-right:50px\">" + getLocalisedTimestamp() + "</td>" +
					"<td style=\"padding-right:50px\">" + getTransaction() + "</td>";
		}

		return toString();
	}
}
