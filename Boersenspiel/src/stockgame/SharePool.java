package stockgame;

import java.util.List;
import java.util.LinkedList;
import stockgame.exceptions.ShareNotFoundException;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * SharePool is holding some Shares
 *
 */

public class SharePool {
	private List<Share> m_shares = new LinkedList<Share>();

	// Neue Aktie im Provider hinterlegen
	public void addShare(Share s) {
		m_shares.add(s);
	}

	// Aktie entfernen
	public void removeShare(Share s) {
		m_shares.remove(s);
	}

	// Sucht eine bestimmte Aktie, ShareNotFoundException falls nicht gefunden
	public Share queryShare(String name) throws ShareNotFoundException {
		Share s = queryShareNoThrow(name);
		if (s == null)
			throw new ShareNotFoundException(name);
		return s;
	}

	// Sucht eine bestimmte Aktie, null falls nicht gefunden
	public Share queryShareNoThrow(String name) {
		int hashCode = name.hashCode();
		for (Share curItem : m_shares) {
			if (curItem.getNameHash() == hashCode)
				return curItem;
		}

		return null;
	}

	// Aktienpool leer?
	public boolean isEmpty() {
		return m_shares.isEmpty();
	}

	// Aktie vorhanden?
	public boolean isShareListed(String shareName) {
		return queryShareNoThrow(shareName) != null;
	}

	public long getCurrentShareRate(String shareName) {
		return queryShare(shareName).getCourse();
	}

	public Share[] getAllShares() {
		return m_shares.toArray(new Share[m_shares.size()]);
	}
}

