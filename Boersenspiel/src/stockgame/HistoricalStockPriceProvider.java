package stockgame;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Map;
import java.util.HashMap;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * File for the HistoricalStockPriceProvider to use the real stock price
 *
 */

public class HistoricalStockPriceProvider extends StockPriceProvider {
	private static class StockPriceInfo {
		public String timePoint; // TODO zu Date �ndern
		public double openCourse;
		public double highCourse;
		public double lowCourse;
		public double closeCourse;
		public long volume;
	}

	private static class StockPriceNode {
		private int m_lastInfoIndex = 0;
		private List<StockPriceInfo> m_priceInfos = new ArrayList<StockPriceInfo>();

		public void addPriceInfo(StockPriceInfo info) {
			m_priceInfos.add(info);
		}

		public StockPriceInfo getNextPriceInfo() {
			if (m_priceInfos.isEmpty())
				return null;

			if (m_lastInfoIndex >= m_priceInfos.size())
				m_lastInfoIndex = 0;

			return m_priceInfos.get(m_lastInfoIndex++);
		}
	}

	private Map<Share, StockPriceNode> m_tokens = new HashMap<Share, StockPriceNode>();
	private static Logger s_logger = Logger.getLogger(HistoricalStockPriceProvider.class.getName());

	public HistoricalStockPriceProvider(SharePool pool) {
		super(pool);
	}

	public void loadStockPriceData(Share share, String fileName) {
		StockPriceNode node = new StockPriceNode();

		try {
			String currentLine = null;
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			reader.readLine(); // Erste Formatzeile �berspringen

			while ((currentLine = reader.readLine()) != null) try {
				StockPriceInfo priceInfo = new StockPriceInfo();
				String[] splitLine = currentLine.split(",");
				if (splitLine.length < 6)
					continue;

				priceInfo.timePoint = splitLine[0];
				priceInfo.openCourse = Double.parseDouble(splitLine[1]);
				priceInfo.highCourse = Double.parseDouble(splitLine[2]);
				priceInfo.lowCourse = Double.parseDouble(splitLine[3]);
				priceInfo.closeCourse = Double.parseDouble(splitLine[4]);
				priceInfo.volume = Long.parseLong(splitLine[5]);
				node.addPriceInfo(priceInfo);
			} catch (Throwable e) {
				s_logger.log(Level.SEVERE, "Fehler beim parsen der CSV-Datei.", e);
			}
		} catch (Throwable e) {
			s_logger.log(Level.SEVERE, "Fehler beim lesen der CSV-Datei.", e);
		}

		m_tokens.put(share, node);
	}

	@Override
	protected void updateShareRate(Share share) {
		StockPriceNode node = m_tokens.get(share);
		if (node == null) return;

		StockPriceInfo priceInfo = node.getNextPriceInfo();
		if (priceInfo == null) return;

		double midCourse = (priceInfo.lowCourse + priceInfo.highCourse) / 2;
		long longCourse = (long) (midCourse * 100);
		share.setCourse(longCourse);
	}
}
