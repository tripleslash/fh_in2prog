package stockgame;

import java.io.PrintStream;
import java.util.logging.Logger;
import java.util.List;
import java.util.LinkedList;

import stockgame.exceptions.*;
import stockgame.transaction.*;
import utils.MimeType;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * Player file which implements the StockGameClient-Interface
 *
 */

public class Player implements IStockGameClient {
	private int m_nameHash;
	private String m_playerName;
	private CashAccount m_cashAccount = null;
	private ShareDepositAccount m_shareDeposit = null;
	private List<SimpleTransaction> m_transactions = new LinkedList<SimpleTransaction>();

	private static Logger s_logger = Logger.getLogger(Player.class.getName());

	public Player(String name, long initialMoney) {
		m_playerName = name;
		m_nameHash = name.hashCode();
		m_cashAccount = new CashAccount(name, initialMoney);
		m_shareDeposit = new ShareDepositAccount(name);
		s_logger.fine("Neuer Spieler angelegt. Name: \"" + name + "\", Guthaben: " + initialMoney);
	}

	// Spielernamen zur�ckgeben
	public String getName() {
		return m_playerName;
	}

	// Hashwert des Spielernamens zur�ckgeben
	public int getNameHash() {
		return m_nameHash;
	}

	// Verm�gen in Aktien zur�ckgeben
	public long getShareValue() {
		return m_shareDeposit.getValue();
	}

	// Verm�gen auf dem Geldkonto zur�ckgeben
	@Override
	public long getMoney() {
		return m_cashAccount.getValue();
	}

	// Gesamtverm�gen des Spielers zur�ckgeben
	public long getValue() {
		// Gesamtverm�gen = Wert des Aktiendepots, Inhalt des Bankaccounts
		return getMoney() + getShareValue();
	}

	// Gibt zur�ck wie viel Gewinn oder Verlust gemacht wurde (mit allen Paketen)
	public long calcTotalWinLossDelta() {
		return m_shareDeposit.calcTotalWinLossDelta();
	}

	// Gibt zur�ck wie viel Gewinn oder Verlust mit einer speziellen Aktie gemacht wurde
	public long calcWinLossDeltaForShare(Share s) {
		return m_shareDeposit.calcWinLossDeltaForShare(s);
	}

	// Gibt alle get�tigten Transaktionen des Spielers zur�ck
	public SimpleTransaction[] getTransactions() {
		return m_transactions.toArray(new SimpleTransaction[m_transactions.size()]);
	}

	// Gibt alle gehaltenen Aktienpakete zur�ck
	@Override
	public ShareItem[] getHeldShareItems() {
		return m_shareDeposit.getHeldShareItems();
	}

	// Schreibt die get�tigten Transaktionen im angegeben Format auf den Stream
	public void writeTransactions(PrintStream stream, MimeType mimeType) {
		switch (mimeType) {
		case TextHtml:
			stream.println("<h1>Transaktionen f&uuml;r \"" + m_playerName + "\":</h1><br>");
			stream.println("<table border=\"1\" rules=\"all\">");
			for (SimpleTransaction trans : m_transactions)
				stream.println("<tr>" + trans.toStringMime(mimeType) + "</tr>");
			stream.println("</table>");
			break;

		default: // text/plain
			for (SimpleTransaction trans : m_transactions)
				stream.println(" - " + trans.toString());
		}
	}

	// Aktienpaket mit n-Aktien kaufen
	@Override
	public void buyShares(Share share, long amount) throws NotEnoughMoneyException {
		// Transaktion mitloggen
		m_transactions.add(new ShareTransaction(ShareTransaction.Type.BuyShares, share, amount));
		s_logger.fine("Aktienkauf von \"" + getName() + "\". " + share + ", St�ckzahl: " + amount);

		// Neues Aktienpaket anlegen, removeMoney wirft NotEnoughMoneyException falls nicht genug Geld vh. ist
		// Deshalb muss addShareItem zwangsl�ufig nach removeMoney aufgerufen werden!
		ShareItem shareItem = new ShareItem(share, amount);
		m_cashAccount.removeMoney(shareItem.getBuyValue());
		m_shareDeposit.addShareItem(shareItem);
	}

	// Bestimmte Aktien aus den Paketen verkaufen
	@Override
	public void sellShares(Share share, long amount) throws NotEnoughSharesException {
		// Transaktion mitloggen
		m_transactions.add(new ShareTransaction(ShareTransaction.Type.SellShares, share, amount));
		s_logger.fine("Aktienverkauf von \"" + getName() + "\". " + share + ", St�ckzahl: " + amount);

		// Solange Aktienpakete vorhanden sind und verkauft werden sollen
		for (ShareItem shareItem : m_shareDeposit.itemsByType(share)) {
			// Das Paket wird vollst�ndig verkauft
			if (shareItem.getCount() <= amount) {
				m_shareDeposit.removeShareItem(shareItem);
				m_cashAccount.addMoney(shareItem.getValue());
				amount -= shareItem.getCount();
			}

			// Es werden nur Teile des Pakets verkauft
			else {
				// Anzahl im Paket anpassen und dem Spieler den Wert der verkauften Teile gutschreiben
				shareItem.setCount(shareItem.getCount() - amount);
				m_cashAccount.addMoney(share.getCourse() * amount);
				amount = 0;
			}
		}

		// Falls am Ende noch Aktien verkauft werden sollen -> nicht genug Aktien vorhanden
		if (amount > 0)
			throw new NotEnoughSharesException();
	}

	@Override
	public String toString() {
		return "Spieler \"" + getName() + "\", Geld: " + getMoney() +
				", Aktienguthaben: " + getShareValue() + ", Gesamtverm�gen: " + getValue();
	}
}

