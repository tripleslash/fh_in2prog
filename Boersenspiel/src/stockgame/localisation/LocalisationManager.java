package stockgame.localisation;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Date;

public class LocalisationManager {
	private Locale m_currentLocale;
	private ResourceBundle m_resourceBundle;
	private static LocalisationManager s_instance = new LocalisationManager(Locale.GERMAN);

	public LocalisationManager(Locale locale) {
		Locale.setDefault(locale);
		m_currentLocale = locale;
		m_resourceBundle = ResourceBundle.getBundle("stockgame", locale);
	}

	public static LocalisationManager getInstance() {
		return s_instance;
	}

	public void setLocale(Locale locale) {
		m_currentLocale = locale;
		m_resourceBundle = ResourceBundle.getBundle("stockgame", locale);
	}

	public Locale getLocale() {
		return m_currentLocale;
	}

	public String getString(String key) {
		return m_resourceBundle.getString(key);
	}

	public String convertDate(Date date) {
		SimpleDateFormat df = new SimpleDateFormat("EEE, MMM d, ''yy hh:mm:ss a", Locale.ENGLISH);
		if (m_currentLocale == Locale.GERMAN)
			df = new SimpleDateFormat("EEE MMM dd kk:mm:ss yyyy", Locale.GERMAN);
		return df.format(date);
	}
}
