package stockgame;

/**
 * 
 * @author Gruppe6
 * @version %I%, %G%
 *
 * ShareItem is a package of Shares
 *
 */

// Aktienpaket das in dem Aktiendepot gehalten wird
public class ShareItem extends Asset {
	// Aktie mit Anzahl und Einkaufswert
	private Share m_share;
	private long m_count;
	private long m_buyValue;

	public ShareItem(Share s, long count) {
		super(s.getName());

		m_share = s;
		m_count = count;
		m_buyValue = s.getCourse() * count;
	}

	// Der Einkaufswert des Aktienpakets
	public long getBuyValue() {
		return m_buyValue;
	}

	// Der aktuelle Wert des Aktienpakets
	public long getValue() {
		return m_share.getCourse() * m_count;
	}

	public Share getShare() {
		return m_share;
	}

	// Die Anzahl der gehaltenen Aktien im Paket
	public long getCount() {
		return m_count;
	}

	// Unterschied aktueller Wert <> Einkaufswert
	public long calcWinLossDelta() {
		return getValue() - getBuyValue();
	}

	// Anzahl der Aktien im Paket ändern
	public void setCount(long count) {
		// Einkaufspreis und Anzahl anpassen
		long diff = count - m_count;
		m_buyValue += diff * m_share.getCourse();
		m_count = count;
	}

	public String toString() {
		return "Aktienpaket \"" + getName() + "\" - Anzahl: " + getCount() +
				", Einkaufspreis: " + getBuyValue() + ", Gesamtwert: " + getValue();
	}
}
